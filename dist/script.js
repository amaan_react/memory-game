const gameContainer = document.getElementById("game");
gameContainer.style.display = "none";


let count = 0;
let countValue;
let score = 0;

const images = [
    "./gifs/1.gif",
    "./gifs/2.gif",
    "./gifs/3.gif",
    "./gifs/5.gif",
    "./gifs/6.gif",
    "./gifs/7.gif",
    "./gifs/8.gif",
    "./gifs/9.gif",
    "./gifs/10.gif",
    "./gifs/11.gif",
    "./gifs/12.gif",
];


const data = document.getElementById("title");
const select = document.querySelector(`select`);

let gameLevel;

select.addEventListener("change", (event) => {

    if (event.target.value == "easy") {
        gameLevel = event.target.value;
        levelSelector(3);
        countValue = 3;
        let shuffledColors = shuffle(imageCount);
        createDivsForColors(shuffledColors);
    }

    if (event.target.value == "medium") {
        gameLevel = event.target.value;
        levelSelector(4);
        countValue = 4;
        let shuffledColors = shuffle(imageCount);
        createDivsForColors(shuffledColors);
    }

    if (event.target.value == "hard") {
        gameLevel = event.target.value;
        levelSelector(5);
        countValue = 5;
        let shuffledColors = shuffle(imageCount);
        createDivsForColors(shuffledColors);
    }

})



let imageCount = [];
function levelSelector(ls) {
    for (let index = 0; index < ls; index++) {
        imageCount.push(images[index]);
        imageCount.push(images[index]);
    }
}


const btn = document.getElementById("reset");
btn.addEventListener("click", (event) => {
    location.reload();
})

btn.style.display = "none";

const label = document.getElementById("choose-level");
const start = document.getElementById("start");

start.addEventListener("click", (event) => {
    
    if (gameLevel == "easy") {
        data.innerHTML = "Your Score : " + score + "," + "  High-Score : " + (localStorage.getItem("easy") | 0);
    } else if (gameLevel == "medium") {
        data.innerHTML = "Your Score : " + score + "," +  "  High-Score : " + (localStorage.getItem("medium") | 0);
    } else {
        data.innerHTML = "Your Score : " + score + "," + "  High-Score : " + (localStorage.getItem("hard") | 0);
    }

    start.style.display = "none";
    select.style.display = "none";
    label.style.display = "none";
    btn.style.display = "block";
    gameContainer.style.display = "flex";
    gameContainer.style.flexWrap = "wrap";
    gameContainer.style.justifyContent = "center"
})


let hasclickedcard = false;
let firstCard, secondCard;


// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}



let shuffledColors = shuffle(images);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {

    for (let color of colorArray) {
        // create a new div
        const newDiv = document.createElement("div");

        // give it a class attribute for the value we are looping over

        newDiv.classList.add(color);

        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleCardClick);

        // append the div to the element with an id of game
        gameContainer.append(newDiv);
    }
}

// TODO: Implement this function!
function handleCardClick(event) {
    score++;

    if (gameLevel == "easy") {
        data.innerHTML = "Your Score : " + score + "," + "  High-Score : " + (localStorage.getItem("easy") | 0);
    } else if (gameLevel == "medium") {
        data.innerHTML = "Your Score : " + score + "," +  "  High-Score : " + (localStorage.getItem("medium") | 0);
    } else {
        data.innerHTML = "Your Score : " + score + "," + "  High-Score : " + (localStorage.getItem("hard") | 0);
    }

    event.target.style.background = `url("${event.target.className}")`;

    if (!hasclickedcard) {
        hasclickedcard = true;
        firstCard = event.target;
        firstCardcolor = event.target.className;
    } else {
        hasclickedcard = false;
        secondCard = event.target;
        secondCardcolor = event.target.className;


        if (firstCardcolor == secondCardcolor) {
            count += 1;
            firstCard.removeEventListener("click", handleCardClick);
            secondCard.removeEventListener("click", handleCardClick);
            if (count == countValue) {
                gameContainer.style.display = "none";
                const winnningNote = document.createElement("h1");
                winnningNote.textContent = "You Won!!";
                winnningNote.style.fontSize = "5em";
                winnningNote.style.textAlign = "center";
                winnningNote.style.marginTop = "1em";
                document.getElementById("body").appendChild(winnningNote);

                if (((gameLevel == "easy") && ((localStorage.getItem("easy"))>score)) || ((localStorage.getItem("easy"))==null)){
                    localStorage.setItem("easy",score)
                    data.innerHTML = "Your Score : " + score + "," + "  High-Score : " + (localStorage.getItem("easy") | 0);
                } else if (((gameLevel == "medium")&& (parseInt(localStorage.getItem("medium"))>score)) || ((localStorage.getItem("medium"))==null)) {
                    localStorage.setItem("medium",score)
                    data.innerHTML = "Your Score : " + score + "," + "  High-Score : " + (localStorage.getItem("medium") | 0);
                } else if (((gameLevel == "hard")&& (parseInt(localStorage.getItem("hard"))>score)) || ((localStorage.getItem("hard"))==null)){
                    localStorage.setItem("hard",score)
                    data.innerHTML = "Your Score : " + score + "," + "  High-Score : " + (localStorage.getItem("hard") | 0);
                }

            }
        } else {

            gameContainer.style.pointerEvents = "none";
            setTimeout(() => {
                firstCard.style.background = "navy";
                secondCard.style.background = "navy";
                gameContainer.style.pointerEvents = "auto";
            }, 1000)

        }
    }
    // you can use event.target to see which element was clicked
    console.log("you clicked", event.target);
}